from tenpy.networks.site import SpinSite
from tenpy.networks.mps import TransferMatrix
from tenpy.models.model import CouplingMPOModel, NearestNeighborModel, CouplingModel, MPOModel
from tenpy.models.lattice import Chain
from tenpy.linalg import np_conserved as npc
import numpy as np
from tenpy.networks.mps import MPS
from tenpy.algorithms import dmrg
from tenpy.tools.params import asConfig
from tenpy.networks.site import kron

""" The implementation from https://tenpy.readthedocs.io/en/latest/intro/simulations.html"""

class AnisotropicSpin1Chain_Pollmann(CouplingMPOModel, NearestNeighborModel):
    r"""An example for a custom model, implementing the Hamiltonian of :arxiv:`1204.0704`.

    .. math ::
        H = J \sum_i \vec{S}_i \cdot \vec{S}_{i+1} + B \sum_i S^x_i + D \sum_i (S^z_i)^2
    """
    default_lattice = Chain
    force_default_lattice = True

    def init_sites(self, model_params):
        B = model_params.get('B', 0.)
        conserve = model_params.get('conserve', 'best')
        if conserve == 'best':
            conserve = 'Sz' if not model_params.any_nonzero(['B']) else None
            self.logger.info("%s: set conserve to %s", self.name, conserve)
        sort_charge = model_params.get('sort_charge', True)
        return SpinSite(S=1., conserve=None, sort_charge=sort_charge)

    def init_terms(self, model_params):
        B = model_params.get('B', 0.)
        D = model_params.get('D', 0.)

        for u1, u2, dx in self.lat.pairs['nearest_neighbors']:
            self.add_coupling(1 / 2., u1, 'Sp', u2, 'Sm', dx, plus_hc=True)
            self.add_coupling(1, u1, 'Sz', u2, 'Sz', dx)

        for u in range(len(self.lat.unit_cell)):
            self.add_onsite(B, u, 'Sx')
            self.add_onsite(D, u, 'Sz Sz')

class AnisotropicSpin1Chain_CouplingModel(MPOModel, CouplingModel,NearestNeighborModel):
    def __init__(self, model_params):
        L = model_params.get('L',2)
        B = model_params.get('B',0)
        D = model_params.get('D',0)
        self.B, self.D = B, D
        bc_MPS = model_params.get('bc_MPS','infinite')
        sort_charge = model_params.get('sort_charge',True)

        conserve = None #'Sz' if B == 0 else 'None' # Conserve Sz if no transverse field
        site = SpinSite(S=1,conserve=conserve,sort_charge=sort_charge)
        site.add_op('Sz2', npc.tensordot(site.Sz,site.Sz,('p*','p')))

        print('B',B,'\t D',D)
                    
        bc = 'open' if bc_MPS == 'finite' else 'periodic'

        self.lat = Chain(L,site,bc=bc,bc_MPS=bc_MPS)

        CouplingModel.__init__(self,self.lat)
        self.add_coupling(0.5,0,'Sp',0,'Sm',1,plus_hc=True) # x-y part of the spin-spin interaction J=1
        self.add_coupling(1,0,'Sz',0,'Sz',1) # z part of the spin-spin interaction
        if B != 0:
            self.add_onsite(B,0,'Sx')
        self.add_onsite(D,0,'Sz2')

        MPOModel.__init__(self,self.lat,self.calc_H_MPO())
        NearestNeighborModel.__init__(self, self.lat, self.calc_H_bond()) 


class AnisotropicSpin1Chain_NNModel(NearestNeighborModel, MPOModel):

    def __init__(self, model_params):
        L = model_params.get('L', 2)
        D = model_params.get('D', 0)

        self.D = D
        bc_MPS = model_params.get('bc_MPS','finite')

        conserve = 'Sz' # We don't implement transverse field here for simplicity
        site = SpinSite(S=1., conserve=conserve)

        # lattice
        bc_MPS = model_params.get('bc_MPS', 'finite')
        bc = 'open' if bc_MPS == 'finite' else 'periodic'
        lat = Chain(L, site, bc=bc, bc_MPS=bc_MPS)

        Sp, Sm, Sz, Id = site.Sp, site.Sm, site.Sz, site.Id
        S_dot_S = (0.5 * (kron(Sp, Sm) + kron(Sm, Sp)) + kron(Sz, Sz)).split_legs().transpose(['p0', 'p1', 'p0*', 'p1*'])
        Sz2_Id = kron(npc.tensordot(Sz,Sz,('p*','p')),Id).split_legs().transpose(['p0', 'p1', 'p0*', 'p1*'])  # On-site Sz^2 term
        Id_Sz2 = kron(Id,npc.tensordot(Sz,Sz,('p*','p'))).split_legs().transpose(['p0', 'p1', 'p0*', 'p1*'])  # On-site Sz^2 termW

        H_bond = [S_dot_S + D*Sz2_Id]*L

        # H_bond[i] acts on sites (i-1, i)
        if bc_MPS == "finite":
            H_bond[0] = None # H[i] acts on site (i-1,i)
            H_bond[-1] += Id_Sz2
            
        # 7) initialize H_bond (the order of 7/8 doesn't matter)
        NearestNeighborModel.__init__(self, lat, H_bond)
        # 9) initialize H_MPO
        MPOModel.__init__(self, lat, self.calc_H_MPO_from_bond())


def m_pollmann_turner_inversion(results, psi, model, simulation, tol=0.01):
    """Measurement function for equation 15 of :arxiv:`1204.0704`.

    See :func:`~tenpy.simulations.measurement.measurement_index` for the call structure.
    """
    psi2 = psi.copy()
    psi2.spatial_inversion()
    mixed_TM = TransferMatrix(psi, psi2, transpose=False, charge_sector=0, form='B')
    evals, evecs = mixed_TM.eigenvectors(which='LM', num_ev=1)
    results['pollmann_turner_inversion_eta'] = eta = evals[0]
    U_I = evecs[0].split_legs().transpose().conj()
    U_I *= np.sqrt(U_I.shape[0])  # previously normalized to npc.norm(U_I) = 1, need unitary
    results['pollmann_turner_inversion_U_I'] = U_I
    if abs(eta) < 1. - tol:
        O_I = 0.
    else:
        O_I = npc.inner(U_I, U_I.conj(), axes=[[0, 1], [1, 0]]) / U_I.shape[0]
    results['pollmann_turner_inversion'] = O_I