# General overview of measurement https://tenpy.readthedocs.io/en/latest/intro/measurements.html
from tenpy.networks.site import SpinSite
from tenpy.networks.mps import TransferMatrix
from tenpy.linalg import np_conserved as npc
import numpy as np
from tenpy.networks.mps import MPS
import h5py
from tenpy.tools import hdf5_io
import glob
import numpy as np
import matplotlib.pyplot as plt
import tenpy.linalg as npc
from analysis_tools import MPS_drop_charge, check_onsite_symmetry, list_contract, fix_norm, check_A4_class

result = []
for fname in glob.glob('./results/dmrg_A4SpinChain_chi_*_L_2.000_lamb_0.865_mu_2.000.h5'):
    with h5py.File(fname) as f:
        data = hdf5_io.load_from_hdf5(f)
        result.append(data)

xi = []
S = []
with open('./correlation_length.csv','w') as f:
    pass
for r in result:
    psi = r['psi']
    print(f'Bond dimension: {psi.chi}')
    xi.append(psi.correlation_length())
    S.append(psi.entanglement_entropy()[0])
    with open('./correlation_length.csv','a') as f:
            f.write(f'{xi[-1]},{S[-1]}\n')

print('Complete')