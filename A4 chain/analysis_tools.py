from tenpy.networks.site import SpinSite
from tenpy.networks.mps import TransferMatrix
from tenpy.linalg import np_conserved as npc
import numpy as np
from tenpy.networks.mps import MPS

# Taken from tenpy forum (I forgot which post is it)
def MPS_drop_charge(psi, charge=None, chinfo=None, permute_p_leg=True):
    import copy, tenpy
    psi_c = psi.copy()
    psi_c.chinfo = chinfo = npc.ChargeInfo.drop(chinfo, charge=charge)
    if permute_p_leg is None and chinfo.qnumber == 0:
        permute_p_leg = True
    for i, B in enumerate(psi_c._B):
        psi_c._B[i] = B = B.drop_charge(charge=charge, chinfo=chinfo)
        psi_c.sites[i] = site = copy.copy(psi.sites[i])
        if permute_p_leg:
            if permute_p_leg is True:
                perm = tenpy.tools.misc.inverse_permutation(site.perm)
            else:
                perm = permute_p_leg
            psi_c._B[i] = B = B.permute(perm, 'p')
        else:
            perm = None
        site.change_charge(B.get_leg('p'), perm) # in place
    print(psi_c.chinfo)
    psi_c.test_sanity()
    return psi_c

def check_onsite_symmetry(psi,op,axes=('p','p*'),num_ev=1,return_eigvec=False,abs_eigval=False):
    #psi = MPS_drop_charge(psi) # Drop charge
    B = [psi.get_B(i) for i in range(psi.L)] # Get all the B matrices
    S = [psi.get_SL(i) for i in range(psi.L+1)] # Get all the SL matrices 
    B_op = [npc.tensordot(b,op,axes) for b in B] # On-site symmetry
    psi_G = MPS(sites=psi.sites,Bs=B_op,SVs=S,bc=psi.bc) # Reconstruct the MPS
    TM = TransferMatrix(bra=psi,ket=psi_G)
    eigval, eigvec = TM.eigenvectors(which='LM',num_ev=num_ev)
    del B,S,B_op # Clean up 
    if abs_eigval:
        eigval = np.abs(eigval)
    if return_eigvec:
        return eigval, eigvec
    return eigval
    
def check_A4_class(psi,eps=1e-5):
    Sx = SpinSite(S=1,conserve='None').Sx
    Sy = SpinSite(S=1,conserve='None').Sy
    Sz = SpinSite(S=1,conserve='None').Sz
    A4 = dict(
        a= npc.tensordot(npc.expm(-.5j*np.pi*Sy),npc.expm(-.5j*np.pi*Sz),('p*','p')), # Rotate about z for pi/2, then about y for pi/2
        e= npc.expm(-1.j*np.pi*Sx) # Rotate about x by pi/2
    )
    eigval_a, eigvec_a = check_onsite_symmetry(psi,A4['a'],return_eigvec=True)
    eigval_e, eigvec_e = check_onsite_symmetry(psi,A4['e'],return_eigvec=True) 

    if np.abs(np.abs(eigval_a[0])-1) > eps or np.abs(np.abs(eigval_e[0])-1) > eps:
        return 0
    Va = fix_norm(eigvec_a[0].split_legs())
    Ve = fix_norm(eigvec_e[0].split_legs())

    VaVxVadVxd = list_contract([Va,Ve,Va.conj().transpose(),Ve.conj().transpose()],[('vL*','vL')]*3)
    VaVxVadVxd2 = npc.tensordot(VaVxVadVxd,VaVxVadVxd,('vL*','vL'))
    return npc.trace(VaVxVadVxd2)/Va.shape[0]

def list_contract(op_list,axes):
    assert len(op_list) == len(axes)+1
    assert len(op_list) >= 2
    temp_op = op_list[0]
    for i in range(1,len(op_list)):
        temp_op = npc.tensordot(temp_op,op_list[i],axes=axes[i-1])
    return temp_op

def fix_norm(V):
    # Given a unitary V that has Tr(Vd V) = 1, fix the normalization such that VVd = I
    return V*np.sqrt(V.shape[0])