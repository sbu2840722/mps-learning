# Detection of $A_4$ spin chain

## Recap of SPT symmetry representation

### Parity transformation 
Consider a parity $\mathcal P$ on the MPS matrices, it has been shown in PhysRevB.81.064439 that under parity
$$\Gamma_m^T = e^{i\theta_I} U_I^\dagger \Gamma_m U_I$$
By iterating the relations, and using the fact that $U_I$ commutes with the Schmidt's values, one can show that
1. $U_I^T = \pm U_I$ (i.e. $U_I$ is either symmetric or antisymmetric)
2. $e^{i\theta_I} = \pm 1$
3. The multiplicity $k_a$ (what does it mean?) must be even
Point (2) can be understood because $\mathcal P \equiv \mathbb Z_2$, and the 1D linear representations must be either $\pm 1$ depending on the parity.
