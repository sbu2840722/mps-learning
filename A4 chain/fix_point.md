# Fix point of transfer operator 
Here we recall a number of useful properties of quantum channel

The quantum channel $T: \mathcal L(V) \to \mathcal L(W)$ between two complex, finite dimensional inner product space is a linear, completely positive, and trace preserving map

## Positive operator are hermitian
Let $\rho$ be a positive operator, i.e. $\braket{\psi|\rho|\psi} \geq 0$, then $\rho$ must be Hermitian

**Proof:** Observe that $\rho = \rho_1 + i \rho_2$ where $\rho_1 = (\rho+\rho^\dagger)/2$ and $\rho_2 = (\rho_1 - \rho_2^\dagger)/2i$ respectively. It is clear that $\rho_1,\rho_2$ are hermitian. Now, take expectation value on both side, since $\braket{\psi|\rho|\psi}\geq 0 \in \mathbb R$ for all $\ket{\psi} \in \mathcal H$, it follows that $\rho_2=0$ and $\rho$ is hermitian. (In fact this can be used to prove that observables must be hermitian operators)

## Hermiticity preservation
An operator $T$ is said to preserve hermiticity if $T(\rho)$ is hermitian if and only if $\rho$ is hermitian. We now prove that this implies $T(\rho)^\dagger = T(\rho^\dagger)$

To show that, we again use the decomposition $\rho= \rho_1 + i\rho_2$. Then is is clear that $$T(\rho)^\dagger = T(\rho_1)^\dagger -i T(\rho_2)^\dagger = T(\rho_1) - i T(\rho_2) = T(\rho_1-i\rho_2)=T(\rho^\dagger)$$
This completes the proof. 

Now let's look at the Choi representation of hermiticity preserving maps. Write , we can see that the hermiticity of $T$ implies that the Choi representation is Hermitian.

## Completely positive maps 
It is useful to introduce the Choi operator $$J(\Phi)=\sum_{i,j} T(\ket{i}\bra{j}) \otimes \ket{i}\bra{j}$$
We now prove a number of important theorems 

### Theorem 1: $T$ is positive iff the Choi operator is positive
**Proof:** Let $X_{ij}\ket{i}