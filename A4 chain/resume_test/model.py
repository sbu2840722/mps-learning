from tenpy.networks.site import SpinSite
from tenpy.networks.mps import TransferMatrix
from tenpy.models.model import CouplingMPOModel, NearestNeighborModel, CouplingModel, MPOModel
from tenpy.models.lattice import Chain
from tenpy.linalg import np_conserved as npc
import numpy as np
from tenpy.networks.mps import MPS
from tenpy.algorithms import dmrg
from tenpy.tools.params import asConfig
from tenpy.networks.site import kron


class A4SpinChain(MPOModel, CouplingModel, NearestNeighborModel):
    def __init__(self, model_params):
        L = model_params.get('L',2)
        lamb = model_params.get('lamb',0)
        mu = model_params.get('mu',0)

        bc_MPS = model_params.get('bc_MPS', 'infinite')
        sort_charge = model_params.get('sort_charge',True)
        conserve = None
        
        site = SpinSite(S=1,conserve=conserve,sort_charge=sort_charge)
        Sx, Sy, Sz=site.Sx, site.Sy, site.Sz
        # Adding a bunch of operators
        site.add_op('Sz2', npc.tensordot(Sz,Sz,('p*','p')))
        site.add_op('Sy2', npc.tensordot(Sy,Sy,('p*','p')))
        site.add_op('Sx2', npc.tensordot(Sx,Sx,('p*','p')))
        site.add_op('SxSy',npc.tensordot(Sx,Sy,('p*','p')),hc='SySx')
        site.add_op('SySx',npc.tensordot(Sy,Sx,('p*','p')),hc='SxSy')
        site.add_op('SxSz',npc.tensordot(Sx,Sz,('p*','p')),hc='SzSx')
        site.add_op('SzSx',npc.tensordot(Sz,Sx,('p*','p')),hc='SxSz')
        site.add_op('SySz',npc.tensordot(Sy,Sz,('p*','p')),hc='SzSy')
        site.add_op('SzSy',npc.tensordot(Sz,Sy,('p*','p')),hc='SySz')
        
        # Build the lattice 
        bc = 'open' if bc_MPS == 'finite' else 'periodic'
        self.lat = Chain(L,site,bc=bc,bc_MPS=bc_MPS)
        CouplingModel.__init__(self,self.lat)

        # (1) The nearest coupling term 
        self.add_coupling(1,0,'Sx',0,'Sx',1) 
        self.add_coupling(1,0,'Sy',0,'Sy',1) 
        self.add_coupling(1,0,'Sz',0,'Sz',1)

        # (2) AKLT like term
        self.add_coupling(mu,0,'Sx2',0,'Sx2',1) 
        self.add_coupling(mu,0,'Sy2',0,'Sy2',1) 
        self.add_coupling(mu,0,'Sz2',0,'Sz2',1)

        # (3) A4 term
        self.add_coupling(lamb,0,'SxSy',0,'Sz',1,plus_hc=True)
        self.add_coupling(lamb,0,'SzSx',0,'Sy',1,plus_hc=True)
        self.add_coupling(lamb,0,'SySz',0,'Sx',1,plus_hc=True)

        self.add_coupling(lamb,0,'Sx',0,'SySz',1,plus_hc=True) 
        self.add_coupling(lamb,0,'Sz',0,'SxSy',1,plus_hc=True) 
        self.add_coupling(lamb,0,'Sy',0,'SzSx',1,plus_hc=True)

        MPOModel.__init__(self,self.lat,self.calc_H_MPO())
        NearestNeighborModel.__init__(self, self.lat, self.calc_H_bond()) 