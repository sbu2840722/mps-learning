# mps-learning
Repository for learning MPS and related physics

## Near term plans
### MPS stuff 
- Implement AKLT states
- How to implement group representation on MPS? More precisely, how to we do representation theory on MPS
- On physics side read a bit about measurement induced phase transitions
- *Implement spin-waves*

### Error correcting codes
- Read the correction code paper recommended by Aswin 
- Read category for physicist paper
- Try to understand the categorical math behind ZX calculus

### Entanglement stuff
- Try to understand how Page wrote down his formula for average entanglement entropy

### CS stuff
- Set up continuous integration for this repository and maybe compile some latex notes 

## Reading list
### MPS
- Detection of gapped phases of a one-dimensional spin chain with on-site and spatial symmetry - Phy Rev. B94, 045136
- Classifying quantum phases using MPS and PEPS - 1010.3732
- Measurement-induced topological entanglement transitions in symmetric random circuits - Nat. Phys. 17, 342-347 (2021)

### ZX / Error correcting code 
- Categories for practising physicist 0905.3010v2

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sbu2840722/mps-learning/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
